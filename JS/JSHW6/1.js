function createNewUser() {
    let newUser = {};

    newUser.firstName = prompt("Введіть ім'я:");
    newUser.lastName = prompt("Введіть прізвище:");
    newUser.birthday = prompt("Введіть дату народження (у форматі dd.mm.yyyy):");

    newUser.getAge = function () {
        const today = new Date();
        const birthDate = new Date(newUser.birthday);
        let age = today.getFullYear() - birthDate.getFullYear();
        const monthDiff = today.getMonth() - birthDate.getMonth();

        if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;
    };

    newUser.getPassword = function () {
        const firstLetter = newUser.firstName.charAt(0).toUpperCase();
        const lastNameLower = newUser.lastName.toLowerCase();
        const birthYear = newUser.birthday.split(".")[2];
        return `${firstLetter}${lastNameLower}${birthYear}`;
    };

    return newUser;
}

const user = createNewUser();

console.log(user);
console.log(`Вік користувача: ${user.getAge()} років`);
console.log(`Пароль користувача: ${user.getPassword()}`);

// 1 Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Экранирование в любых програмах - это механизм, который позволяет вводить специальные символы (экранирование) для интерпретации других символов в рядах или символических литературах. Это означает, что используются специальные символы, такие как рисы, подсказки и одинарные лапки, которые можно было бы правильно обработать компилятором или интерпретатором.


// 2 Які засоби оголошення функцій ви знаєте?
// Функціональне виразне оголошення: var myFunction = function() { /* код функції */ };
// Оголошення за допомогою function: function myFunction() { /* код функції */ }
// Стрілкова функція (ES6+): const myFunction = () => { /* код функції */ };



// 3 Що таке hoisting, як він працює для змінних та функцій?
// Hoisting - це механізм в JavaScript, при якому оголошення змінних і функцій піднімаються вгору своєї області видимості перед виконанням коду. Це означає, що ви можете викликати функції або використовувати змінні навіть перед їхнім фактичним оголошенням. Проте, важливо враховувати, що підняття застосовується тільки до оголошень, а не до ініціалізації (присвоєння значень).
